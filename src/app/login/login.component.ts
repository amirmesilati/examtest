import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  hide = true;
  public errorMessage:string;
  
  constructor(public authService:AuthService,private router:Router) { 
    this.authService.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
    authService.err = "";
  }
  
  onSubmit(){
    this.authService.login(this.email,this.password);
    
  }

  ngOnInit() {
  }

}
