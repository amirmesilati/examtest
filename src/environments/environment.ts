// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
  apiKey: "AIzaSyBMoZa4AuSfcNNBqrfMf5eNZgxHCvk958k",
  authDomain: "examtest-db501.firebaseapp.com",
  databaseURL: "https://examtest-db501.firebaseio.com",
  projectId: "examtest-db501",
  storageBucket: "examtest-db501.appspot.com",
  messagingSenderId: "483021324502",
  appId: "1:483021324502:web:8229a49374a63e97148977",
  measurementId: "G-PRE5L8P8MB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
